#include "ros/ros.h"
#include "simple_ros_service/AddTwoInts.h"

bool add(simple_ros_service::AddTwoInts::Request  &req,
         simple_ros_service::AddTwoInts::Response &res )
{
  res.sum = req.a + req.b;
  ROS_INFO("request: x=%ld, y=%ld", (long int)req.a, (long int)req.b);
  ROS_INFO("  sending back response: [%ld]", (long int)res.sum);
  return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "add_two_ints_server");
  ros::NodeHandle n;

  ros::ServiceServer service = n.advertiseService("add_two_ints", add);

  ros::spin();

  return 0;
}
